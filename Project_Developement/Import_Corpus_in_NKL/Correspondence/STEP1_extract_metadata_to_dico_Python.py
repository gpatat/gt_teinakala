"""
- author: Gwenaëlle Patat
- date: November 2021
- description: Extracting metadata from XML files into a python dictionnary
- input: XML files
- source code : https://github.com/FloChiff/DAHNProject/blob/master/Project%20development/Scripts/Digitization/extract_metadata_from_files.py

"""

import os.path
from bs4 import BeautifulSoup

# This script must be adjusted to the structure of each corpus in XML-TEI with the metadata wanted for the project.

path_corpus = "./XML_files/"
#print(os.listdir(path_corpus))
for root, dirs, files in os.walk(path_corpus) :
    for filename in files :
        # That b in the mode specifier in the open() states that the file shall be treated as binary, so contents will remain a bytes
        with open(path_corpus + filename, 'rb') as xml_file:
            soup = BeautifulSoup(xml_file, 'xml')
            
            lettres = soup.find_all('TEI')
            list_dico_meta = {}
            list_dico_meta["file"] = filename
            # 2 values possible : published or pending
            list_dico_meta["status"] = "pending"
            # Collection must to be created before the datas import
            list_dico_meta["collection"] = "DOI du corpus sur Nakala"
            # Type's value must to be equal to a URI
            list_dico_meta["type"] = "http://purl.org/coar/resource_type/c_0857"
            list_dico_meta["license"] = "CC-BY-NC-SA-4.0"
            list_dico_meta["publisher"] = "Université de Bretagne Occidentale, Brest, 2021"
            # Users lists must to be created before the datas import
            list_dico_meta["rights"] = "id_liste_equipe, ROLE"
        
            # The matches must to be adapted to the structure of the encoded file
            try:
                
                title = soup.find("title").get_text()
                #print(title)
                list_dico_meta["title"] = title

                contributors = soup.find_all("respStmt")
                for contributor in contributors :
                    name_contributor = contributor.find("persName").get_text()    

                    #print(name_contributor)
                    #dico_meta["Contributor"] = name_contributor
                    list_dico_meta["contributor"] = contributors

                desc_source = soup.find("msDesc")
                source = desc_source.find("msIdentifier").get_text()
                #print(source)
                list_dico_meta["source"] = source

                desc_phys = desc_source.find("physDesc")
                description1 = desc_phys.find("objectDesc").get("form")
                #print(description1)

                desc_hand = desc_phys.find("handDesc")
                description2 = desc_hand.find("handNote").get_text()
                #print(description2)

                description3 = desc_phys.find("accMat")
                if description3 != None:
                    description3 = description3.get_text()
                else:
                    description3 = "Not given"
    
                abstract = soup.find("abstract")
                #print(abstract)
                description4 = abstract.find("p")
                if description4 != None:
                    description4 = description4.get_text()
                else:
                    description4 = "Not given"
                    
                
                list_dico_meta["description"] = [description1, description2, description3, description4]
    
                additional = soup.find("additional")
                #print(additional)
                biblio = additional.find("bibl")
                if biblio != None:
                    biblio = biblio.get_text()
                else:
                    biblio = "Not given"
                    
                list_dico_meta["bibliographicCitation"] = biblio
    
                correspAction = soup.find("correspAction", attrs={"type": "sent"})
                #print(correspAction)
                creator = correspAction.find("persName").get_text()
                #print(creator)
                list_dico_meta["creator"] = creator

                correspAction = soup.find("correspAction", attrs={"type": "sent"})
                #print(correspAction)
                created = correspAction.find("date").get("when")
                #print(created)
                list_dico_meta["created"] = created

                correspAction = soup.find("correspAction", attrs={"type": "received"})
                #print(correspAction)
                audience = correspAction.find("persName").get_text()
                #print(audience)
                list_dico_meta["audience"] = audience
                    
                keywords = soup.find("keywords")
                #print(keywords)
                subject = keywords.find_all("term")
                #print(subject)
                    
                list_dico_meta["subject"] = subject
                
                language_transcription = soup.find("div", attrs={"type": "transcription"})
                #print(language_transcription)
                language = language_transcription.get("xml:lang")
                #print(language)
                list_dico_meta["language"] = language
                    
            except :
                print("bad string: {}".format(lettres))
                continue
                
            print(list_dico_meta) 