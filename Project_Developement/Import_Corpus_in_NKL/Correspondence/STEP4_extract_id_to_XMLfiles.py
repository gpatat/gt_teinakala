"""
- author: Gwenaëlle Patat
- date: Jannuary 2022
- description: Extracting datas (DOI and URI of the images) from Nakala to XML-TEI files
- input: Nakala's API
- source code : GitLab Michael Nauge, NakalaPyConnect
"""

# Endpoint API & API's key
apiUrl = 'https://apitest.nakala.fr'
# User test unakala3
apiKey = 'aae99aba-476e-4ff2-2886-0aaf1bfa6fd2'

# url de Nakala en test : bac à sable
BASE_URL = "https://test.nakala.fr"
#API_URL = "https://apitest.nakala.fr"



# un dictionnaire permettant de faciliter l'attribution d'un data_type
# pour des humains.
VOCABTYPE = {
    "image" : "http://purl.org/coar/resource_type/c_c513",
    "video" : "http://purl.org/coar/resource_type/c_12ce",
    "son" : "http://purl.org/coar/resource_type/c_18cc",
    "article de journal" : "http://purl.org/coar/resource_type/c_6501",
    "poster/présentation en conférence" : "http://purl.org/coar/resource_type/c_6670",
    "contribution à une conférence" : "http://purl.org/coar/resource_type/c_c94f",
    "objet d'apprentissage" : "http://purl.org/coar/resource_type/c_e059",
    "ouvrage" : "http://purl.org/coar/resource_type/c_2f33",
    "carte géographique" : "http://purl.org/coar/resource_type/c_12cd",
    "jeu de données encodé et structuré" : "http://purl.org/coar/resource_type/c_ddb1",
    "logiciel" : "http://purl.org/coar/resource_type/c_5ce6",
    "autre" : "http://purl.org/coar/resource_type/c_1843",
    "fonds d'archives" : "http://purl.org/library/ArchiveMaterial",
    "exposition d'art" : "http://purl.org/ontology/bibo/Collection",
    "bibliographie" : "http://purl.org/coar/resource_type/c_86bc",
    "bulletin" : "http://purl.org/ontology/bibo/Series",
    "édition de sources" : "http://purl.org/coar/resource_type/c_ba08",
    "manuscrit" : "http://purl.org/coar/resource_type/c_0040",
    "correspondance" : "http://purl.org/coar/resource_type/c_0857",
    "rapport" : "http://purl.org/coar/resource_type/c_93fc",
    "périodique" : "http://purl.org/coar/resource_type/c_2659",
    "prépublication" : "http://purl.org/coar/resource_type/c_816b",
    "recension" : "http://purl.org/coar/resource_type/c_efa0",
    "partition" : "http://purl.org/coar/resource_type/c_18cw",
    "données d'enquête" :"https://w3id.org/survey-ontology#SurveyDataSet",
    "texte" : "http://purl.org/coar/resource_type/c_18cf",
    "thèse" : "http://purl.org/coar/resource_type/c_46ec",
    "page web" :  "http://purl.org/coar/resource_type/c_7ad9",
    "data paper" : "http://purl.org/coar/resource_type/c_beb9",
    "article programmable" : "http://purl.org/coar/resource_type/c_e9a0"
    }


VOCABTYPE_reverse = {
    "http://purl.org/coar/resource_type/c_c513" : "image",
    "http://purl.org/coar/resource_type/c_12ce" : "video",
    "http://purl.org/coar/resource_type/c_18cc" : "son",
    "http://purl.org/coar/resource_type/c_6501" : "article de journal", 
    "http://purl.org/coar/resource_type/c_6670" : "poster/présentation en conférence",
    "http://purl.org/coar/resource_type/c_c94f" : "contribution à une conférence",
    "http://purl.org/coar/resource_type/c_e059" : "objet d'apprentissage",
    "http://purl.org/coar/resource_type/c_2f33" : "ouvrage",
    "http://purl.org/coar/resource_type/c_12cd" : "carte géographique",
    "http://purl.org/coar/resource_type/c_ddb1" : "jeu de données encodé et structuré",
    "http://purl.org/coar/resource_type/c_5ce6" : "logiciel",
    "http://purl.org/coar/resource_type/c_1843" : "autre",
    "http://purl.org/library/ArchiveMaterial" : "fonds d'archives",
    "http://purl.org/ontology/bibo/Collection" : "exposition d'art",
    "http://purl.org/coar/resource_type/c_86bc" : "bibliographie",
    "http://purl.org/ontology/bibo/Series" : "bulletin",
    "http://purl.org/coar/resource_type/c_ba08" : "édition de sources",
    "http://purl.org/coar/resource_type/c_0040" : "manuscrit",
    "http://purl.org/coar/resource_type/c_0857" : "correspondance",
    "http://purl.org/coar/resource_type/c_93fc" : "rapport",
    "http://purl.org/coar/resource_type/c_2659" : "périodique",
    "http://purl.org/coar/resource_type/c_816b" : "prépublication", 
    "http://purl.org/coar/resource_type/c_efa0" : "recension",
    "http://purl.org/coar/resource_type/c_18cw" : "partition",
    "https://w3id.org/survey-ontology#SurveyDataSet" : "données d'enquête",
    "http://purl.org/coar/resource_type/c_18cf" : "texte",
    "http://purl.org/coar/resource_type/c_46ec" : "thèse",
    "http://purl.org/coar/resource_type/c_7ad9" : "page web",
    "http://purl.org/coar/resource_type/c_beb9" : "data paper",
    "http://purl.org/coar/resource_type/c_e9a0" : "article programmable"
    }

import requests
import json

class NakalaException(Exception):
    pass

def get_datas_collection_byId(collectionId, page, limit):
    """
    Obtenir toutes les données d'une collection nakala dont on connait le doi
    

    Parameters
    dataId
    dataTitle : STR
        un identifier nakala.

    Returns
    -------
    dicoMetas :DICT
        un dictionnaire contenant les metadatas obtenus depuis la reponse json du server
        

    """   
    
    # https://apitest.nakala.fr/collections/10.34847/nkl.09844sfj
        
    url = apiUrl + "/collections/" + collectionId + "/datas"

    APIheaders = {"X-API-KEY": apiKey} 
    
    try : 
        
        response = requests.get(url, headers=APIheaders)
        if response.status_code == 200 :
            
            dicoMetas = json.loads(response.text)
        
            return dicoMetas
        else:
            print(str(response))
            raise NakalaException(response.text)
        
    except NakalaException:
        print("error request") 
        
collectionId = "10.34847/nkl.09844sfj"
#print(get_datas_collection_byId(collectionId))
# Tentative de changer le nombre de pages et de données à afficher
page = 1
limit = 555

response = get_datas_collection_byId(collectionId, page, limit)
print(response)      
#datas_DOI = response[metas]


# Créer un dictionnaire python récupérant dans fichier json le titre des données de la collection associé à son DOI. 

import json

dict_dataTitle_DOI = {}

with open("list_datas_collection_Test.json") as f:
    datas_collection = json.loads(f.read())
    #print(datas_collection)
    datas = datas_collection["data"][0:-1]
    #print(datas)
    
    #data_identifier = datas_collection["data"][0]["identifier"]
    #data_title = datas_collection["data"][0]["metas"][0]["value"]  
    
    #for version in datas_collection :
        #print(version, ":", datas_collection["data"][0]["version"])
        
    #print(datas_collection["data"][0]["identifier"])
    for idx,data in enumerate(datas) :
        #print(data)
        data_identifier = data["identifier"]
        #print(data_identifier)
        if data["metas"][0]["propertyUri"] == "http://nakala.fr/terms#title" :
            #print(data["metas"][0]["value"])
            data_title = data["metas"][0]["value"]               
            dict_dataTitle_DOI = {data_title : data_identifier}          
                
            print(dict_dataTitle_DOI)