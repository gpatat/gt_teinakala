# Dépôt du groupe de travail TEI-Nakala

Le groupe de travail TEI-Nakala réunit des ingénieurs·es et chercheurs·ses issu·e·s des correspondants Huma-Num, des membres de la Plateforme Scripto du Réseau national des Maisons des Sciences de l'Homme, ainsi que de laboratoires de recherche. Né d'un besoin de fluidifier l'insertion de l'entrepôt de données Nakala dans différents environnements de publication TEI, l'objectif de ce groupe de travail est de conceptualiser une plateforme d'expertise permettant la mise en place, la maintenance et la ré-appropriation d'une pipeline de traitement de corpus en XML-TEI, de leur conception à leur publication en passant par le dépôt dans Nakala. 

Les livrables engagés par le groupe sont les suivants : 
- Proposer des recommandations claires à soumettre à la communauté afin de faciliter le dépôt de fichiers XML-TEI en lot dans Nakala. Les enjeux sont de réfléchir à un _data mapping_ adéquat des métadonnées TEI vers les champs Dublin Core, et de pouvoir insérer les DOI générés par Nakala (au niveau de la donnée et de la ou les collections) vers les fichiers d’encodage en TEI ;
- Concevoir des scripts prêts à l'emploi pour le _data mapping_ et le dépôt de fihcier en lot dans Nakala ;
- Modéliser les différents environnements de publication de fichiers en XML-TEI afin de mieux visualiser les connecteurs qui seraient utiles à développer.

Le dépôt est organisé de la manière suivante :
- le dossier `Metadata_Recommendations` renvoie aux réflexions du GT sur la manière d'harmoniser nos pratiques quant à la compréhension des métadonnées de l'entrepôt Nakala
- le dossier `Project_Developement` contient les fichiers de développement pour le fonctionnement de la plateforme envisagée
- le dossier `Schemas` regroupe les différents schémas et modèles produits dans le cadre des différentes réflexions du GT
- le dossier `Workflow_Presentations` réunit les supports de présentations montrés lors de colloques ou séminaires

