<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="application/xml"
	schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
   <teiHeader>
      <fileDesc>
         <titleStmt>
            <title>Title</title>
         </titleStmt>
         <publicationStmt>
            <p>Publication Information</p>
         </publicationStmt>
         <sourceDesc>
            <p>Information about the source</p>
         </sourceDesc>
      </fileDesc>

      <!-- Renseigner le caractère obligatoire ou facultatif des champs (du point de vue de Nakala). -->
      <!-- Désambiguïser les champs DC et les multiplier/compléter si besoin -->
      <!-- Qu'est-ce qui va dans <dcterms:creator> et <dcterms:contributor> ? L'éditeur scientifique peut-il aller dans dcterms:creator ? Cf. PDN : distinction de l'éditeur matériel et de l'éditeur scientifique. Éditeur matériel placé dans champ dcterms:publisher et plusieurs options pour éditeur scientifique : dcterms:creator car participe à la création de la ressource décrite ? dcterms:mediator car sert d’intermédiaire pour rendre la ressource accessible ? dcterms:contributor pour montrer que l’éditeur a contribué à la ressource, ou bien son rôle est-il plus engageant qu’une contribution ? -->
      <!-- Citation depuis Nakala : modèle de datacite, citation du point de vue de la donnée numérique et non de la source d'origine. Mettre dans les métadonnées les infos de la ressource num et dans titre le max d’infos sur la ressource d’origine (à diviser en sous-titres) ? -->
      <!-- Dans cas d'une édition en TEI dérivée d'une œuvre non-nativement numérique : garder l'idée du double <xenodata> pour 2 dépôts, l'un renvoyant à la ressource physique (document numérisé), l'autre à l'édition numérique avec les fichiers en XML, avec un lien entre les 2 via un sous-champ du dcterms:relation ? -->
      <!-- Penser à la normalisation de l'information : manière de renseigner les valeurs des champs. -->
      <!-- Penser à indiquer une différenciation apparente dans Nkl pour les mêmes champs répétés (types de dcterms:description, types de dcterms:types, types de dcterms:relation ...). @type ? -->


      <!-- Bloc de métadonnées du point de vue de le ressource numérique. Citation dans Nakala : Auteur de la ressource numérique + date de dépôt + Titre de la donnée + Type (numérique) + NAKALA + DOI -->
      <xenoData type="digital_data">
         <oai_dc:dcterms xmlns:dc="http://purl.org/dc/elements/1.1/"
            xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
            xmlns:dcterms="http://purl.org/dc/terms/">
            <!-- Champs pour créer la donnée -->
            <!-- nakala:title -->
            <dcterms:title lang="format_ISO_639-1">Obligatoire. Titre de la ressource. Faire autant
               de champs titres que nécessaires selon arborescence du document encodé. Valeur :
               "Édition numérique de [telle œvre déposée] réalisée dans le cadre du projet [nom du
               projet]".</dcterms:title>
            <!-- Format de nakala:creator dans Nakala : Nom Prénom -->
            <dcterms:creator>Obligatoire. Entité responsable de la création de la ressource :
               auteur, éditeur (chercheur.se, ingénieur.e, etc.)... Faire autant de champs creator
               que nécessaires. Format de la valeur dans nkl:creator (à garder uniforme pour chaque
               auteur) : Nom, Prénom.</dcterms:creator>
            <!-- Format de nakala:created dans Nakala : AAAA ou AAAA-MM ou AAAA-MM-JJ -->
            <dcterms:created>Obligatoire. Date de création ou d'édition ? Entendu comme le champ
               dcterms:created dans Nakala, au sens création du contenu de la ressource et pas de sa
               forme numérisée. Pas la date de numérisation ou de mise en ligne (date du dépôt de la
               donnée automatiquement calculée par Nkl). Valeur : Uniquement format AAAA-MM-JJ
               accepté (norme W3CDTF).</dcterms:created>
            <!-- nakala:type. Type d'ordre numérique ici (image, texte, video, son, set de données, ...) -->
            <dcterms:type type="datatype">Obligatoire. Type de donnée déposée. Valeur : Liste issue
               du référentiel Types de COAR (confederation of open access repositories),
               dcterms:URI.</dcterms:type>
            <dcterms:license>Obligatoire. Licence attribuée à la ressource. Valeur : Code des
               licences disponibles sur Nakala.</dcterms:license>
            <dcterms:available typeUri="http://purl.org/dc/terms/W3CDTF"
               propertyUri="http://purl.org/dc/terms/available">Facultatif. Date de mise en ligne et
               donc de fin d'embargo. Valeur : normes W3CDTF.</dcterms:available>
            <dcterms:dateAccepted typeUri="http://purl.org/dc/terms/W3CDTFl"
               propertyUri="http://purl.org/dc/terms/dateAccepted">Facultatif. Date d'approbation de
               la ressource (par exemple pour thèse ou article ou édition numérique approuvée par un
               éditeur matériel). Valeur : normes W3CDTF</dcterms:dateAccepted>
            <dcterms:issued typeUri="http://purl.org/dc/terms/W3CDTF"
               propertyUri="http://purl.org/dc/terms/issued">Facultatif. Date de publication de la
               ressource. Valeur : normes W3CDTF.</dcterms:issued>
            <dcterms:modified
               typeUri="http://purl.org/dc/terms/W3CDTF|http://purl.org/dc/terms/Period"
               propertyUri="http://purl.org/dc/terms/modified">Facultatif. Date où un changement a
               été effectué sur la ressource. cf. revisionDesc en TEI. Valeur : normes W3CDTF ou
               DCMI Period.</dcterms:modified>
            <dcterms:contributor lang="format_ISO_639-1"
               typeUri="http://www.w3.org/2001/XMLSchema#string|http://www.w3.org/1999/02/22-rdf-syntax-ns#PlainLiteral"
               propertyUri="http://purl.org/dc/terms/contributor">Facultatif. Entité reponsable de
               contributions apportées à la ressource. Bien distinguer les rôles de chacun :
               Éditeur(s) ? Chargé(s) d'édition ? Traducteur ? Financeur ? Institutions partenaires
               ? Faire autant de champs contributor que nécessaires. Valeur : Au moins Nom, Prénom
               (rôle) + éventuellement institution de rattachement, identifiant ORCID. Format
               xsd:string ou rdf:PlainLiteral.</dcterms:contributor>
            <dcterms:publisher
               typeUri="http://www.w3.org/2001/XMLSchema#string|http://www.w3.org/1999/02/22-rdf-syntax-ns#PlainLiteral"
               propertyUri="http://purl.org/dc/terms/publisher">Obligatoire. Entité ayant particpé
               aux finacements (Institutions de rattachement des participants aux projet). Valeur :
               Libre. Format xsd:string ou rdf:PlainLiteral.</dcterms:publisher>
            <!--<dcterms:rights lang="format_ISO_639-1"
               typeUri="http://www.w3.org/2001/XMLSchema#string|http://www.w3.org/1999/02/22-rdf-syntax-ns#PlainLiteral"
               propertyUri="http://purl.org/dc/terms/rights">Facultatif. Répartition des droits sur
               la donnée. Valeur : identifiant d'un utilisateur Nakala,
               ROLE_READER/ROLE_EDITOR/ROLE_ADMIN. Format xsd:string ou
               rdf:PlainLiteral.</dcterms:rights> -->
            <dcterms:rights typeUri="http://www.w3.org/2001/XMLSchema#anyURI"
               propertyUri="http://purl.org/dc/terms/rights">Facultatif. Information concernant la
               nature des droits qui s'appliquent à la donnée. La bonne pratique est d'utiliser un
               URI de référence.</dcterms:rights>
            <dcterms:rightsHolder lang="format_ISO_639-1"
               typeUri="http://www.w3.org/2001/XMLSchema#string|http://www.w3.org/1999/02/22-rdf-syntax-ns#PlainLiteral"
               propertyUri="http://purl.org/dc/terms/rightsHolder"> Facultatif. Personne ou
               organisation qui détient ou gère les droits sur la donnée.</dcterms:rightsHolder>
            <dcterms:source lang="format_ISO_639-1"
               typeUri="http://www.w3.org/2001/XMLSchema#string|http://www.w3.org/1999/02/22-rdf-syntax-ns#PlainLiteral"
               propertyUri="http://purl.org/dc/terms/source">Facultatif. Soit une édition nativement
               numérique ; des références bibliographiques ; une édition princeps ; une édition de
               sources ; une édition critique. Valeur : Libre (cote de la référence ou DOI si
               existant). Format xsd:string ou rdf:PlainLiteral.</dcterms:source>
            <dcterms:format lang="format_ISO_639-1" typeUri="http://purl.org/dc/terms/URI"
               propertyUri="http://purl.org/dc/terms/format">Facultatif. Description du format de la
               ressource numérisée. Valeur : Liste issue du référentiel Media Types MIME (cf.
               https://www.iana.org/assignments/media-types/media-types.xhtml).
               dcterms:URI.</dcterms:format>
            <dcterms:type lang="format_ISO_639-1" typeUri="http://purl.org/dc/terms/URI"
               propertyUri="http://purl.org/dc/terms/type">Facultatif. Type de texte déposé. Valeur
               : Utiliser de préférence un vocabulaire normalisé (cf. Thésaurus Cahier "Typologies
               textuelles") avec comme format dcterms:URI (URI issu d'opentheso).</dcterms:type>
            <!-- Langues sur interface graphique de Nakala -->
            <dcterms:language>Facultatif. Langue de la ressource. Faire autant de champs que
               nécessaires. Valeur : normes ISO-639-1 et ISO-639-3.</dcterms:language>
            <!-- Mots-clés sur interface graphique de Nakala -->
            <dcterms:subject lang="format_ISO_639-1">Facultatif. Mots-clés, codes de
               classification,... Utiliser de préférence un vocabulaire contrôlé issu des
               référentiels moissonnés par Isidore ou du thesaurus "typologie textuelle"
               d'OpenTheso. Valeur : Libre.</dcterms:subject>
            <!-- Description sur interface graphique de Nakala -->
            <dcterms:description type="desc_project">Facultatif. Description du projet et de ses
               objectifs. Valeur : Libre en commençant par "Description du projet : ". Format
               xsd:string ou rdf:PlainLiteral.</dcterms:description>
            <!-- Description sur interface graphique de Nakala -->
            <dcterms:description type="desc_processing">Facultatif. Description de la chaîne de
               traitement appliquée au corpus (outils, environnements de traitement et de
               publication). Cf. appInfo. Valeur : Libre ou bien choisir dans liste déroulante
               d'environnements ? en commençant par " Description de la chaîne de traitement
               appliquée au corpus : ". Liste à définir. Format xsd:string ou
               rdf:PlainLiteral.</dcterms:description>
            <dcterms:abstract lang="format_ISO_639-1"
               typeUri="http://www.w3.org/2001/XMLSchema#string|http://www.w3.org/1999/02/22-rdf-syntax-ns#PlainLiteral"
               propertyUri="http://purl.org/dc/terms/abstract">Facultatif. Résumé de la ressource.
               Valeur : Libre. Format xsd:string ou rdf:PlainLiteral.</dcterms:abstract>
            <dcterms:isPartOf
               typeUri="http://www.w3.org/2001/XMLSchema#string|http://www.w3.org/1999/02/22-rdf-syntax-ns#PlainLiteral"
               propertyUri="http://purl.org/dc/terms/isPartOf">Obligatoire (pour obliger à la
               création de collections en amont du dépôt du corpus). Faire autant de champs que de
               collections concernées. Valeur : nom, identifiant de la collection associée,
               xsd:id.</dcterms:isPartOf>
            <dcterms:isReferencedBy typeUri="http://purl.org/dc/terms/URI"
               propertyUri="http://purl.org/dc/terms/isReferencedBy">Facultatif. Site du projet où
               la donnée est éditée. Valeur : URL, dcterms:URL.</dcterms:isReferencedBy>
            <!-- Champs pour mettre à jour la donnée (en lien avec d'autres déjà déposées) -->
            <dcterms:relation typeUri="http://www.w3.org/2001/XMLSchema#ID"
               propertyUri="http://purl.org/dc/terms/relation">Facultatif. Lien vers une autre
               donnée déposée dans Nakala et associée à la ressource. Valeur : DOI,
               xsd:id.</dcterms:relation>
            <dcterms:identifier>Après dépôt de la donnée sur Nkl. DOI attribué par Nkl [à renseigner
               en sortie après un 1er dépôt].</dcterms:identifier>
            <dcterms:conformsTo typeUri="http://www.w3.org/2001/XMLSchema#ID"
               propertyUri="http://purl.org/dc/terms/conformsTo">Obligatoire. Valeur : DOI de la
               donnée contenant le schéma d'encodage RNG et éventuellement la documentation ODD,
               xsd:id.</dcterms:conformsTo>
            <dcterms:isVersionOf typeUri="http://www.w3.org/2001/XMLSchema#ID"
               propertyUri="http://purl.org/dc/terms/isVersionOf">Obligatoire dans le cas d'un
               double dépôt pour une même œuvre : la source numérique (l'édition numérique en TEI
               par exemple) qui sera ici reliée à son origine matérielle (DOI du document numérisé),
               xsd:id</dcterms:isVersionOf>
         </oai_dc:dcterms>
      </xenoData>


      <!-- Bloc de métadonnées du point de vue de le ressource physique d'où est dérivée la ressource numérique. Citation souhaitée dans Nakala : Auteur de la ressource physique + date de création de la ressource d'origine + Titre de la donnée + Type (physique) + NAKALA + DOI -->
      <xenoData type="physical_resource">
         <oai_dc:dcterms xmlns:dc="http://purl.org/dc/elements/1.1/"
            xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
            xmlns:dcterms="http://purl.org/dc/terms/">
            <!-- Champs pour créer la donnée -->
            <!-- nakala:title -->
            <dcterms:title lang="format_ISO_639-1">Obligatoire. Titre de la ressource. Faire autant
               de champs titres que nécessaires selon arborescence du document encodé. Valeur :
               Titre de l'œuvre (privilégier le titre officiel donné à la ressource si'il existe).
               Mettre entre crochets si le titre est forgé par l'équipe.</dcterms:title>
            <!-- Format de nakala:creator dans Nakala : Nom Prénom -->
            <dcterms:creator>Obligatoire. Entité responsable de la création de la ressource :
               auteur, éditeur... Faire autant de champs creator que nécessaires. Valeur : cf.
               formats VIAF-ISNI-ARK, forme normalisée du nom. Format de la valeur dans nkl:creator
               (à garder uniforme pour chaque auteur) : Nom, Prénom. Si la mention de l'auteur ne
               permet pas de respecter ce format, indiquer "Anonyme" dans le premier élément
               dcterms:creator du xenodata, puis doubler le champ avec une autre forme normalisée du
               nom (privilégier des formes de référencement approuvées comme formats
               VIAF-ISNI-ARK).</dcterms:creator>
            <!-- Format de nakala:created dans Nakala : AAAA ou AAAA-MM ou AAAA-MM-JJ -->
            <dcterms:created>Obligatoire. Date de création ou d'édition ? Entendu comme le champ
               dcterms:created dans Nakala, au sens création du contenu de la ressource et pas de sa
               forme numérisée. Pas la date de numérisation ou de mise en ligne (date du dépôt de la
               donnée automatiquement calculée par Nkl). Valeur : Uniquement format AAAA-MM-JJ
               accepté (norme W3CDTF). Si la date de création n'est pas connue précisément, indiquer
               "Inconnue" et ajouter les hypothèses (intervalles de dates) dans un deuxième champ
               dcterms:created.</dcterms:created>
            <dc:created typeUri="http://purl.org/dc/terms/Period"
               propertyUri="http://purl.org/dc/terms/created">2e champ created pour les dates
               hypothétiques ou moins précises. Valeur : Respecter norme DCMI period, cf.
               https://www.dublincore.org/specifications/dublin-core/dcmi-period/. name=valeur;
               start=valeur; end=valeur; scheme=valeur;</dc:created>
            <!-- nakala:type. Type d'ordre physique ici : correspondance, livre, édition de source, manuscrit, etc. -->
            <dcterms:type type="sourcetype">Obligatoire. Type de donnée déposée. Valeur : Liste
               issue du référentiel Types de COAR (confederation of open access repositories),
               dcterms:URI.</dcterms:type>
            <dcterms:license>Obligatoire. Licence attribuée à la ressource. Valeur : Code des
               licences disponibles sur Nakala.</dcterms:license>
            <dcterms:available typeUri="http://purl.org/dc/terms/W3CDTF"
               propertyUri="http://purl.org/dc/terms/available">Facultatif. Date de mise en ligne et
               donc de fin d'embargo. Valeur : normes W3CDTF.</dcterms:available>
            <dcterms:dateAccepted typeUri="http://purl.org/dc/terms/W3CDTF"
               propertyUri="http://purl.org/dc/terms/dateAccepted">Facultatif. Date d'approbation de
               la ressource (par exemple pour thèse ou article). Valeur : normes
               W3CDTF</dcterms:dateAccepted>
            <dcterms:dateCopyrighted typeUri="http://purl.org/dc/terms/W3CDTF"
               propertyUri="http://purl.org/dc/terms/dateCopyrighted">Facultatif. Date du copyright,
               en général l'année de la publication. Valeur : normes
               W3CDTF.</dcterms:dateCopyrighted>
            <dcterms:dateSubmitted typeUri="http://purl.org/dc/terms/W3CDTF"
               propertyUri="http://purl.org/dc/terms/dateSubmitted">Facultatif. Date de soumission
               d'une ressource en attente de validation (par exemple pour thèse ou article). Valeur
               : normes W3CDTF.</dcterms:dateSubmitted>
            <dcterms:issued typeUri="http://purl.org/dc/terms/W3CDTF"
               propertyUri="http://purl.org/dc/terms/issued">Facultatif. Date de publication de la
               ressource. Valeur : normes W3CDTF.</dcterms:issued>
            <dcterms:contributor lang="format_ISO_639-1"
               typeUri="http://www.w3.org/2001/XMLSchema#string|http://www.w3.org/1999/02/22-rdf-syntax-ns#PlainLiteral"
               propertyUri="http://purl.org/dc/terms/contributor">Facultatif. Entité reponsable de
               contributions apportées à la ressource. Bien distinguer les rôles de chacun :
               co-auteurs ? Éditeur(s) ? Chargés d'édition ? Traducteur ? Financeur ? Institutions
               partenaires ? Faire autant de champs creator que nécessaires. Valeur : Au moins Nom,
               Prénom + institution de rattachement, identifiant Viaf, ISNI, Ark ou Orcid. Format
               xsd:string ou rdf:PlainLiteral.</dcterms:contributor>
            <dcterms:publisher
               typeUri="http://www.w3.org/2001/XMLSchema#string|http://www.w3.org/1999/02/22-rdf-syntax-ns#PlainLiteral"
               propertyUri="http://purl.org/dc/terms/publisher">Obligatoire. L'éditeur matériel et
               le lieu de publication. Valeur : Libre. Format xsd:string ou
               rdf:PlainLiteral.</dcterms:publisher>
            <dcterms:type lang="format_ISO_639-1" typeUri="http://purl.org/dc/terms/URI"
               propertyUri="http://purl.org/dc/terms/type">Facultatif. Type de texte déposé. Valeur
               : Utiliser de préférence un vocabulaire normalisé (cf. Thésaurus Cahier "Typologies
               textuelles") avec comme format dcterms:URI (URI issu d'opentheso).</dcterms:type>
            <dcterms:format lang="format_ISO_639-1" typeUri="http://purl.org/dc/terms/URI"
               propertyUri="http://purl.org/dc/terms/format">Facultatif. Description du format de la
               ressource numérisée. Valeur : Liste issue du référentiel Media Types MIME (cf.
               https://www.iana.org/assignments/media-types/media-types.xhtml).
               dcterms:URI.</dcterms:format>
            <dcterms:source lang="format_ISO_639-1"
               typeUri="http://www.w3.org/2001/XMLSchema#string|http://www.w3.org/1999/02/22-rdf-syntax-ns#PlainLiteral"
               propertyUri="http://purl.org/dc/terms/source">Facultatif. Soit une édition nativement
               numérique ; des références bibliographiques ; une édition princeps ; une édition de
               sources ; une édition critique. Valeur : Libre (cote de la référence ou DOI si
               existant). Format xsd:string ou rdf:PlainLiteral.</dcterms:source>
            <dcterms:medium lang="format_ISO_639-1"
               typeUri="http://www.w3.org/2001/XMLSchema#string|http://www.w3.org/1999/02/22-rdf-syntax-ns#PlainLiteral"
               propertyUri="http://purl.org/dc/terms/medium">Description matérielle de la ressource
               (avec indication du supports, des mains, etc.). Libre. Format xsd:string ou
               rdf:PlainLiteral.</dcterms:medium>
            <!--<dcterms:rights lang="format_ISO_639-1"
               typeUri="http://www.w3.org/2001/XMLSchema#string|http://www.w3.org/1999/02/22-rdf-syntax-ns#PlainLiteral"
               propertyUri="http://purl.org/dc/terms/rights">Facultatif. Répartition des droits sur
               la donnée. Valeur : identifiant d'un utilisateur Nakala,
               ROLE_READER/ROLE_EDITOR/ROLE_ADMIN. Format xsd:string ou
               rdf:PlainLiteral.</dcterms:rights> -->
            <dcterms:rights typeUri="http://www.w3.org/2001/XMLSchema#anyURI"
               propertyUri="http://purl.org/dc/terms/rights">Facultatif. Information concernant la
               nature des droits qui s'appliquent à la donnée. La bonne pratique est d'utiliser un
               URI de référence.</dcterms:rights>
            <dcterms:rightsHolder lang="format_ISO_639-1"
               typeUri="http://www.w3.org/2001/XMLSchema#string|http://www.w3.org/1999/02/22-rdf-syntax-ns#PlainLiteral"
               propertyUri="http://purl.org/dc/terms/rightsHolder"> Facultatif. Personne ou
               organisation qui détient ou gère les droits sur la donnée.</dcterms:rightsHolder>
            <!-- Langues sur interface graphique de Nakala -->
            <dcterms:language>Facultatif. Langue de la ressource. Faire autant de champs que
               nécessaires. Valeur : normes ISO-639-1 et ISO-639-3.</dcterms:language>
            <!-- Mots-clés sur interface graphique de Nakala -->
            <dcterms:subject>Facultatif. Mots-clés, codes de classification,... Utiliser de
               préférence un vocabulaire contrôlé issu des référentiels moissonnés par Isidore ou du
               thesaurus "typologie textuelle" d'OpenTheso. Valeur : Libre.</dcterms:subject>
            <!-- Description sur interface graphique de Nakala -->
            <dcterms:description type="desc_project">Facultatif. Description du projet et de ses
               objectifs. Valeur : Libre en commençant par "Description du projet : ". Format
               xsd:string ou rdf:PlainLiteral.</dcterms:description>
            <dcterms:abstract lang="format_ISO_639-1"
               typeUri="http://www.w3.org/2001/XMLSchema#string|http://www.w3.org/1999/02/22-rdf-syntax-ns#PlainLiteral"
               propertyUri="http://purl.org/dc/terms/abstract">Facultatif. Résumé de la ressource.
               Valeur : Libre. Format xsd:string ou rdf:PlainLiteral.</dcterms:abstract>
            <dcterms:bibliographicCitation lang="format_ISO_639-1"
               typeUri="http://www.w3.org/2001/XMLSchema#string|http://www.w3.org/1999/02/22-rdf-syntax-ns#PlainLiteral"
               propertyUri="http://purl.org/dc/terms/bibliographicCitation">Facultatif. Référence
               bibliographique pour la ressource. Faire autant de champs que de références. Valeur :
               Libre. Format xsd:string ou rdf:PlainLiteral.</dcterms:bibliographicCitation>
            <dcterms:isReferencedBy typeUri="http://purl.org/dc/terms/URI"
               propertyUri="http://purl.org/dc/terms/isReferencedBy">Facultatif. Site du projet où
               la donnée est éditée. Valeur : URL, dcterms:URL.</dcterms:isReferencedBy>
            <dcterms:isPartOf
               typeUri="http://www.w3.org/2001/XMLSchema#string|http://www.w3.org/1999/02/22-rdf-syntax-ns#PlainLiteral"
               propertyUri="http://purl.org/dc/terms/isPartOf">Facultatif. Collection ou revue à
               laquelle appartient la ressource. Valeur : Libre. Format xsd:string ou
               rdf:PlainLiteral.</dcterms:isPartOf>
            <dcterms:audience lang="format_ISO_639-1"
               typeUri="http://www.w3.org/2001/XMLSchema#string|http://www.w3.org/1999/02/22-rdf-syntax-ns#PlainLiteral"
               propertyUri="http://purl.org/dc/terms/audience">Facultatif. Destintaire d'une lettre
               dans cas d'une correspondance par exemple. Valeur : Libre. Format xsd:string ou
               rdf:PlainLiteral.</dcterms:audience>
            <dcterms:isPartOf
               typeUri="http://www.w3.org/2001/XMLSchema#string|http://www.w3.org/1999/02/22-rdf-syntax-ns#PlainLiteral"
               propertyUri="http://purl.org/dc/terms/isPartOf">Obligatoire (pour obliger à la
               création de collections en amont du dépôt du corpus). Faire autant de champs que de
               collections concernées. Valeur : nom, identifiant de la collection associée,
               xsd:id.</dcterms:isPartOf>
            <!-- Champs pour mettre à jour la donnée (en lien avec d'autres déjà déposées) -->
            <dcterms:relation typeUri="http://www.w3.org/2001/XMLSchema#ID"
               propertyUri="http://purl.org/dc/terms/relation">Facultatif. Lien vers une autre
               donnée déposée dans Nakala et associée à la ressource. Valeur : DOI,
               xsd:id.</dcterms:relation>
            <dcterms:identifier>Après dépôt de la donnée sur Nkl. DOI attribué par Nkl [à récupérer
               après un 1er dépôt non publié].</dcterms:identifier>
            <dcterms:hasVersion typeUri="http://www.w3.org/2001/XMLSchema#ID"
               propertyUri="http://purl.org/dc/terms/hasVersion">Obligatoire dans le cas d'un double
               dépôt pour une même œuvre : la source physique (document numérisé) qui sera ici
               reliée à sa version numérique (DOI de l'édition numérique en TEI par exemple,
               xsd:id).</dcterms:hasVersion>
         </oai_dc:dcterms>
      </xenoData>
   </teiHeader>
   <text>
      <body>
         <p>Some text here.</p>
      </body>
   </text>
</TEI>
